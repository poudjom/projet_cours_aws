import mysql.connector
from mysql.connector import Error
import pymysql as pymysql

import cx_Oracle

def liste_table_bd_oracle():
    try:
        con = cx_Oracle.connect('hr', 'manager', '15.237.150.117/xe')
        cursor = con.cursor()
        cursor.execute("SELECT owner, table_name FROM all_tables where owner = 'HR'")
        print("\nListe des tables de la base HR")
        i = 0
        for row in cursor:
            i += i + 1
            print(' -> ', row[1])
    except Error as e:
        print("Error while connecting to MySQL", e)
    finally:
        cursor.close()
        con.close()


def copie_des_donnees(table):
    try:
        #connection = mysql.connector.connect(host='192.168.1.75', database='hr', user='aris', password='aris')
        con = cx_Oracle.connect('hr', 'manager', '15.237.150.117/xe')
        connection = mysql.connector.connect(host='aurora-db-write-instance-1.czdkf2ovliak.eu-west-3.rds.amazonaws.com', database='hr', user='admin', password='administrateur')
        cursor = con.cursor()

        if table.upper() == 'JOBS':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                requete = "insert into jobs (JOB_ID, JOB_TITLE, MIN_SALARY, MAX_SALARY) values('{0}','{1}',{2},{3})".format(row[0], row[1], row[2], row[3])
                cursor = connection.cursor()
                cursor.execute(requete)
            print("\n**** Données de la table jobs copiées avec succès **** ")

        elif table.upper() == 'EMPLOYEES':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                requete = "insert into employees (EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NUMBER, HIRE_DATE, JOB_ID, SALARY) " \
                          "values({0},'{1}','{2}','{3}','{4}','{5}','{6}',{7})" \
                    .format(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7])
                cursor = connection.cursor()
                cursor.execute(requete)
            print("\n**** Données de la table employees copiées avec succès **** ")

        elif table.upper() == 'DEPARTMENTS':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                if row[2] == None:
                    manager = 'NULL'
                else:
                    manager = row[2]
                requete = "insert into DEPARTMENTS (DEPARTMENT_ID,DEPARTMENT_NAME, MANAGER_ID, LOCATION_ID) values({0},'{1}',{2},{3})"\
                        .format(row[0], row[1], manager, row[3])
                cursor = connection.cursor()
                cursor.execute(requete)
            print("\n**** Données de la table département copiées avec succès **** ")

        elif table.upper() == 'REGIONS':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                requete = "insert into REGIONS (REGION_ID, REGION_NAME) values({0},'{1}')"\
                        .format(row[0], row[1])
                print(requete)
                cursor1 = connection.cursor()
                cursor1.execute(requete)
            print("\n**** Données de la table régions copiées avec succès **** ")

        elif table.upper() == 'LOCATIONS':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                if row[4] == None:
                    state = 'NULL'
                else:
                    state = row[4]
                requete = "insert into LOCATIONS (LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID) " \
                          "values({0},'{1}', '{2}', '{3}', '{4}', '{5}')" .format(row[0], row[1], row[2], row[3], state, row[5])
                #print(requete)
                cursor = connection.cursor()
                cursor.execute(requete)
            print("\n**** Données de la table localisation copiées avec succès **** ")

        elif table.upper() == 'JOB_HISTORY':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                requete = "insert into JOB_HISTORY (EMPLOYEE_ID, START_DATE, END_DATE, JOB_ID, DEPARTMENT_ID) " \
                          "values({0},'{1}', '{2}', '{3}', {4})" .format(row[0], row[1], row[2], row[3], row[4])
                cursor = connection.cursor()
                cursor.execute(requete)
            print("\n**** Données de la table job_history copiées avec succès **** ")

        elif table.upper() == 'COUNTRIES':
            cursor = con.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                requete = "insert into COUNTRIES (COUNTRY_ID, COUNTRY_NAME, REGION_ID) " \
                          "values('{0}','{1}', {2})" .format(row[0], row[1], row[2])
                cursor = connection.cursor()
                cursor.execute(requete)
            print("\n**** Données de la table country copiées avec succès **** ")
        connection.commit()
    except Error as e:
        print("Error while connecting to MySQL", e)
    finally:
        cursor.close()
        con.close()
        connection.close()


def contenu_writer(table, base):
    try:
        if base == 'writer':
            #connection = mysql.connector.connect(host='192.168.1.75', database='hr', user='aris', password='aris')
            connection = mysql.connector.connect(host='aurora-db-write-instance-1.czdkf2ovliak.eu-west-3.rds.amazonaws.com', database='hr', user='admin', password='administrateur')
        elif base == 'reader':
            1
            #connection = mysql.connector.connect(host='aurora-db-write-instance-1.czdkf2ovliak.eu-west-3.rds.amazonaws.com', database='hr', user='admin', password='administrateur

        if table.upper() == 'JOBS':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
        elif table.upper() == 'EMPLOYEES':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
        elif table.upper() == 'DEPARTMENTS':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
        elif table.upper() == 'REGIONS':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
        elif table.upper() == 'LOCATIONS':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
        elif table.upper() == 'JOB_HISTORY':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
        elif table.upper() == 'COUNTRIES':
            cursor = connection.cursor()
            cursor.execute("SELECT * from {0}".format(table.upper()))
            for row in cursor:
                print(row)
    except Error as e:
        print("Error while connecting to MySQL", e)
    finally:
        cursor.close()
        connection.close()


def vider_tables():
    try:
        connection = mysql.connector.connect(host='aurora-db-write-instance-1.czdkf2ovliak.eu-west-3.rds.amazonaws.com', database='hr', user='admin', password='administrateur')
        cursor = connection.cursor()
        cursor.execute("delete from REGIONS;")
        cursor.execute("delete from LOCATIONS")
        cursor.execute("delete from DEPARTMENTS")
        cursor.execute("delete from JOBS")
        cursor.execute("delete from EMPLOYEES")
        cursor.execute("delete from JOB_HISTORY")
        cursor.execute("delete from COUNTRIES")
        connection.commit()
        cursor.close()
        connection.close()
        print("\n**** Tables vidées avec succès. **** ")
    except Error as e:
        print("Error while connecting to MySQL", e)




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    while True:
        print("\n ############################################################################################## \n")
        print("A- Afficher les tables la base oracle")
        print("B- Copier les données d'une table oracle vers le SGBD MYSQL")
        print("C- Vider le contenu des tables MYSQL")
        print("D- Afficher le contenu d'une table de la base writer")
        print("E- Afficher le contenu d'une table de la base reader")
        print("F- Sortir du programme")
        choix = input("\n=> Saisir votre choix: ")
        if (choix == 'A'):
            liste_table_bd_oracle()
        elif(choix == 'B'):
            table = input("\n=> Saisir la nom de la table à copier : ")
            copie_des_donnees(table)
        elif (choix == 'C'):
            vider_tables()
        elif (choix == 'D'):
            table = input("\n=> Saisir la nom de la table à afficher : ")
            contenu_writer(table, 'writer')
        elif (choix == 'F'):
            print("\n Merci. Aurevoir!")





# See PyCharm help at https://www.jetbrains.com/help/pycharm/
